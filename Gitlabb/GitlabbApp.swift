//
//  GitlabbApp.swift
//  Gitlabb
//
//  Created by  Sergei on 30.12.2020.
//

import SwiftUI

@main
struct GitlabbApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
